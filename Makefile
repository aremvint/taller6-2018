CC = gcc
CFLAGS = -O2 -Wall -I .

# This flag includes the Pthreads library on a Linux box.
# Others systems will probably require something different.


all: taller6

taller6: taller6_bad.c 
	$(CC) $(CFLAGS) -g -o taller6 taller6_bad.c 

clean:
	rm -f *.o taller6 *~

